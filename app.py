from sqlite3.dbapi2 import Cursor
from flask import Flask, app, g
import sqlite3
from flask import render_template
from werkzeug.wrappers import request
from flask import request



app = Flask(__name__) 

def connect_db():
    sql = sqlite3.connect('./database.db')
    sql.row_factory = sqlite3.Row
    return sql

def get_db():
    if not hasattr(g,'sqlite3'):
        g.sqlite3_db = connect_db()
    return g.sqlite3_db

@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()

@app.route('/')
def index():
    return '<h1>pour un fois, cela fonctionne magnifique</h1>'
 
@app.route('/form')
def form():
    return render_template('form.html')    

@app.route('/auteur')
def viewauteur():
    db = get_db()
    cursor = db.execute('SELECT auteur_id, name, date FROM auteur')
    resultats = cursor.fetchall()
    return f"<h1> L'auteur id et {resultats[0]['auteur_id']}. <br> Le nom et {resultats[0]['name']}. <br> Quel et la date {resultats[0]['date']}. <br>"

@app.route('/create', methods = ['POST'])
def create():
    db = sqlite3.connect('database.db')
    cursor = db.cursor()

    message = request.form.get('Notes')

    cursor = db.execute('INSERT INTO message(message) VALUES("%s")' % (message))
    db.commit()

    db.close()
    return 'message enregistré'

@app.route('/listes')
def listes():
    db = get_db()
    cursor = db.execute('SELECT message_id, message FROM message')
    resultats = cursor.fetchall()
    return f"<h1> Le message id et {resultats[0]['message_id']}. <br> Le message et {resultats[0]['message']}."




if __name__ == '__main__':
    app.run(debug=True)
